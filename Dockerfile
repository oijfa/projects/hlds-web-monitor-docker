FROM ruby:2.5-alpine

#RUN apt-get update -qq && apt-get install -y build-essential git
RUN apk add git

ENV APP_HOME /app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

RUN git clone https://gitlab.com/oijfa/projects/hlds-web-monitor.git .
RUN gem install bundler
RUN bundle install 

ADD . $APP_HOME

EXPOSE 81

CMD ["bundle", "exec", "rackup", "--host", "0.0.0.0", "-p", "81"]
